export PATH $PATH:$HOME/bin

alias .. 'cd ..'
alias ... 'cd ../..'
alias .... 'cd ../../..'
alias current_branch 'git symbolic-ref HEAD | cut -d / -f 3'
alias df 'df -h'
alias ducks 'du -cksh * | sort -rn|head -11'
alias empties 'find . -empty -type d -maxdepth 2'
alias gd 'git diff'
alias gdc 'git diff --cached'
alias gdh 'git diff HEAD'
alias gdm 'git diff origin/master'
alias gr 'grep -r'
alias gs 'git status'
alias ip 'ifconfig | grep '\''inet '\'''
alias ls 'ls -1 --color=auto'
alias systail 'tail -f /var/log/system.log'
alias v 'vim'

function cd {
    builtin cd $@
    [ -f ".config" ] && . .config
}
