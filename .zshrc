# More configuration
autoload -U colors && colors
setopt HIST_IGNORE_ALL_DUPS

export LS_COLORS='no=00;37:fi=00:di=00;33:ln=04;36:pi=40;33:so=01;35:bd=40;33;01:'
export PROMPT="%{$fg[red]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg_no_bold[yellow]%}%1~ %{$reset_color%}%# "
export PATH=$PATH:$HOME/bin

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias current_branch='git symbolic-ref HEAD | cut -d / -f 3'
alias df='df -h'
alias ducks='du -cksh * | sort -rn|head -11'
alias empties='find . -empty -type d -maxdepth 2'
alias gd='git diff'
alias gdc='git diff --cached'
alias gdh='git diff HEAD'
alias gdm='git diff origin/master'
alias gr='grep -r'
alias gs='git status'
alias ip='ifconfig | grep '\''inet '\'''
alias ls='ls -1 --color=auto'
alias systail='tail -f /var/log/system.log'
alias v='vim'

function cd {
    builtin cd "$@"
    [ -f ".config" ] && source .config
}
