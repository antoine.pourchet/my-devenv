#! /bin/bash

DIRNAME="$(dirname "$(readlink -f "$0")")"

# General installs
sudo apt-add-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install -y git
sudo apt-get install -y vim
sudo apt-get install -y python
sudo apt-get install -y python-pip
sudo apt-get install -y ruby
sudo apt-get install -y golang
sudo apt-get install -y texlive-base
sudo apt-get install -y texlive-latex-extra
sudo apt-get install -y terminator
sudo apt-get install -y xzdec
sudo apt-get install -y zsh
sudo apt-get install -y jq
sudo apt-get install -y fish

# Node setup
sudo apt-get install -y npm
sudo apt-get install -y nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node

# Sublime setup
sudo add-apt-repository ppa:webupd8team/sublime-text-2
sudo apt-get update
sudo apt-get install -y sublime-text
sudo sh -c 'echo "#! /bin/bash\n/opt/sublime_text_2/sublime_text $1 &> /dev/null &" > /usr/bin/subl'
sudo chmod +x /usr/bin/subl

# Docker setup
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo sh -c 'echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list'
sudo apt-get update
sudo apt-get purge lxc-docker
sudo apt-cache policy docker-engine

sudo apt-get install -y docker-engine
sudo service docker start

# Apache Benchmark setup
sudo apt-get install -y apache2-utils

# SSH setup
pushd ~/.ssh/ && ssh-keygen -t rsa -b 4096 -C "antoine.pourchet@gmail.com" && popd

# Git setup
git config --global user.name "Antoine Pourchet"
git config --global user.email "antoine.pourchet@gmail.com"
echo "Now make sure git has your public key"

# ~/bin setup
[ -d "$HOME/bin" ] && rm -r $HOME/bin
ln -s $DIRNAME/bin $HOME

# ZSH setup
chsh -s /bin/zsh
ln -s $DIRNAME/.zshrc $HOME

# FISH setup
mkdir -p $HOME/.config/fish
ln -s $DIRNAME/config.fish $HOME/.config/fish/config.fish

# VIM setup
[ -L "$HOME/.vim_runtime" ] && rm $HOME/.vim_runtime
ln -s $DIRNAME/.vim_runtime $HOME
sh $HOME/.vim_runtime/install_basic_vimrc.sh

