" Make sure we get back to same line
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Remaps
inoremap $$ $$<Esc>i

" Other settings
set nu
set nohlsearch
set autochdir
set cursorline

set wildmenu
set lazyredraw
set ruler
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set autoindent
set expandtab
set exrc
set mouse=
